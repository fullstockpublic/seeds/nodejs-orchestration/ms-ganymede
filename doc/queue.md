# Queue

**Porque mensajeria?**

Uno de los grandes problemas entre comunicacion de micro-servicios son:

* Espera excesica ante una respuesta ('Time Out')
* Posibilidad de afectar otros procesos de la aplicacion
* Posibilidad de error del servidor, donde no se pueda recuperar y se pierda el request con su job.

Por lo que se implementa el concepto:

```mermaid
graph TD
A>Producer]
B(Consumer)
C[Work]
D[Result]
A -- Assigns --> C
B -- Get Started  --> C
B -- Finish --> D
```

IDE - [p3x-redis-ui](https://electronjs.org/apps/p3x-redis-ui)

