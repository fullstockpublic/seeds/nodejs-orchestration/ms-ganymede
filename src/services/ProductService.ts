import { Service } from 'typedi';
import { ObjectID } from 'typeorm';
import { OrmRepository, InjectRepository } from "typeorm-typedi-extensions";

import { OrderRepository } from '../orm/repositories/OrderRepository';
import { Product } from '../orm/entities/Product';
import { ProductRepository } from '../orm/repositories/ProductRepository';
import { ProductModel } from '../models/ProductModel';
import { Status } from '../orm/entities/Enums/Status';

@Service("ProductService")
export class ProductService {
    @OrmRepository() private productRepository: ProductRepository;
    @OrmRepository() private orderRepository: OrderRepository;

    constructor() {
    }

    GetList(): Promise<Product[]> {
       return this.productRepository.find();
    }    

    async GetListFromOrder(idOrder: ObjectID): Promise<Product[]> {
        
        let order = await this.orderRepository.findOne(idOrder);
        
        return this.productRepository.find({ idOrder: order.id.toString() });
    } 

    Get(id: ObjectID): Promise<Product> {
        return this.productRepository.findOne(id);
    }   

    async GenerateFromListSearch(productModel: ProductModel[], idOrder: string) {
        
        let order = await this.orderRepository.findOne(idOrder);

        await this.productRepository.CreationFromthemisto(productModel, idOrder);

        order.status = Status.PROCESSED;

        await this.orderRepository.save(order);

    }

}