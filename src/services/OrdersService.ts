import { Service } from 'typedi';
import { ObjectID } from 'typeorm';
import { OrmRepository, InjectRepository } from "typeorm-typedi-extensions";

import { IOrdersService } from "./abstract/IOrdersService"
import { Order } from '../orm/entities/Order';
import { OrderRepository } from '../orm/repositories/OrderRepository';

@Service("OrdersService")
export class OrdersService {
    
    @OrmRepository() private orderRepository: OrderRepository;

    constructor() {
    }

    GetList(): Promise<Order[]> {
       return this.orderRepository.find();
    }    

    Get(id: ObjectID): Promise<Order> {
        return this.orderRepository.findOne(id);
    }   

}