import "reflect-metadata";
import * as dotenv from "dotenv";
import Container from 'typedi';
import {useContainer as typeormUseContainer, ConnectionOptions, Connection, createConnection} from "typeorm";
import { useContainer as routingUseContainer } from "routing-controllers";

if (process.env.NODE_ENV !== "production") {
    // Used only in development to load environment variables from local file.
    dotenv.config();
}

import { queues, Ganymede } from './amqp/Queue';
import { ProductService } from "./services/ProductService";
import connectionOpts from "./orm/Database";

try {
    typeormUseContainer(Container);
    console.log("Run");
    
    queues[Ganymede].process("OrderNotification", async (job, done) => {
       await createConnection(connectionOpts).then(async connection => { 
            console.log(`Job Run with result`);
            let puppeteerService = Container.get(ProductService);
            if (job.data.productList != null) {
                console.log(`Job completed with result ${JSON.stringify(job.data.productList)}`);

                await puppeteerService.GenerateFromListSearch(job.data.productList, job.data.id);
                      
                console.log(`Job finish with OrderNotification ${JSON.stringify(job.data.id)}`);

                done();

            } else {
                const errorLogic = `your work does not contain the OrderNotification. `;
                done(new Error(errorLogic));
                console.log(errorLogic + ` ${JSON.stringify(job.data)}`);
            } 
        })
        .catch( (error) => {            
            done(error);
            console.log(`Job Error with ${error}`);
            }            
        );        
    });
    queues[Ganymede].on('completed', (job, result) => {
        console.log(`Job completed with result ${result}`);
    });
    queues[Ganymede].on('error', function(error) {
        console.log(`Job completed with result ${error}`);
    });
    queues[Ganymede].on('Active', function(error) {
        console.log(`Job completed with result ${error}`);
    });
} catch (error) {
    console.log(`Catch |`);
    console.log(error);
}

