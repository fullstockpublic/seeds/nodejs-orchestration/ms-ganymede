import { Authorized, Body, JsonController, Post, QueryParam, QueryParams, Param, Get, BadRequestError } from 'routing-controllers';
import { ObjectID } from 'typeorm';
import Container, { Inject } from 'typedi';

import { ProductService } from '../services/ProductService';


@JsonController('/')
export class ProductController {

    @Inject("ProductService") private productService: ProductService;


    @Get()
    public async GetList() {                
        return await this.productService.GetList();
    }

    @Get("/:id")
    public async Get(@Param('id') id: ObjectID) {
        if(id == null)        
            throw new BadRequestError("id Product is Null");
        
        return this.productService.Get(id);
    }

    @Get("/ForOrderId")
    public async GetForOrderId(@Param('idorder') idorder: ObjectID) {
        if(idorder == null)        
            throw new BadRequestError("id Order is Null");
        
        return this.productService.GetListFromOrder(idorder);
    }


}