import { Authorized, Body, JsonController, Post, QueryParam, QueryParams, Param, Get, BadRequestError } from 'routing-controllers';
import { ObjectID } from 'typeorm';
import Container, { Inject } from 'typedi';

import { OrdersService } from '../services/OrdersService';


@JsonController('/orders')
export class OrdersController {

    @Inject("OrdersService") private ordersService: OrdersService;


    @Get()
    public async GetList() {                
        return this.ordersService.GetList();
    }

    @Get("/:id")
    public async Get(@Param('id') id: ObjectID) {
        if(id == null)        
            throw new BadRequestError("id Order is Null");
        
        return this.ordersService.Get(id);
    }


}