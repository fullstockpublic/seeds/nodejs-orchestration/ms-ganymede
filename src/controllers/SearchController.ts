import { Authorized, Body, JsonController, Post, QueryParam, QueryParams, Param, Get, BadRequestError, Controller } from 'routing-controllers';
import { ObjectID } from 'typeorm';

import { Search } from '../orm/entities/Search';
import { SearchService } from '../services/SearchService';
import { Order } from '../orm/entities/Order';
import { queues, Themisto } from '../amqp/Queue';


@Controller('/search')
export class SearchController {

    constructor(private ordersService: SearchService) {}

    @Post()
    public async Post(@Body() search: Search) {
        if(search == null)        
            return new BadRequestError("id Order is Null");
        
        let order: Order = await this.ordersService.Post(search);

        //Producers Task 
        queues[Themisto].add("Search", {
            id: order.id,
            search: search
            });

        return order;
    }    
}