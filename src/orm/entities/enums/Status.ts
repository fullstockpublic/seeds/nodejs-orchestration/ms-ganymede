export enum Status {
    RECEIVED = 0,
    PROCESSING = 1,
    FAILED = 2,
    PROCESSED = 3
}