import { Column, Entity, ObjectID, ObjectIdColumn } from "typeorm";
import { Transform } from 'class-transformer';
import toHexString from "../../tools/toHexString";
import { UserOption } from "./UserOption";

@Entity()
export class Search {

    @ObjectIdColumn()
    @Transform(toHexString, {toPlainOnly: true})
    id: ObjectID;

    @Column()
    searchQuery: string;

    @Column()
    provider: string;

    @Column()
    callbackUrl: string;

    //TODO : user:password entity? Security Null :P
    userOption: UserOption;
}
