import {Column, Entity, ObjectID, ObjectIdColumn, ManyToOne} from "typeorm";
import { ProductCategory } from "./ProductCategory";
import { Order } from "./Order";
import { Transform } from 'class-transformer';
import toHexString from "../../tools/toHexString";

@Entity()
export class Product {

    @ObjectIdColumn()
    @Transform(toHexString, {toPlainOnly: true})
    id: ObjectID;

    @Column()
    typeCodeIdentification: string;

    @Column()
    codeIdentification: string;

    @Column()
    description: string;

    @Column()
    name: string;

    @Column()
    price: string;

    @Column()
    discount: string;

    @Column()
    usedSearchTerm: string;

    @Column()
    imagesURL: string[];

    @Column()
    idOrder: string;
/* 
    @Column(type => ProductCategory)
    productCategory: ProductCategory;  
    */

}
