import { Column, Entity, ObjectID, ObjectIdColumn, OneToOne, JoinColumn } from "typeorm";
import { Transform } from 'class-transformer';
import { Status } from "./Enums/Status";
import { Search } from "./Search";
import toHexString from "../../tools/toHexString";

@Entity()
export class Order {

    @ObjectIdColumn()
    @Transform(toHexString, {toPlainOnly: true})
    id: ObjectID;

    @Column()
    status: Status;

    @Column(type => Search)
    search: Search;

}
