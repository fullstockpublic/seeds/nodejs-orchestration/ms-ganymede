import { EntityRepository, FindManyOptions, Repository, getRepository } from 'typeorm';
import { Product } from '../entities/Product';
import { ProductModel } from '../../models/ProductModel';
import { Service } from 'typedi';

@Service()
@EntityRepository(Product)
export class ProductRepository extends Repository<Product> {

  public async CreationFromthemisto(productModel: ProductModel[], idOrder: string) {
        
    for (const key in productModel) {

      const product = new Product();
      product.codeIdentification = productModel[key].CodeIdentification;
      product.typeCodeIdentification = productModel[key].typeCodeIdentification;
      product.discount = productModel[key].discount;
      product.description = productModel[key].description;
      product.name = productModel[key].name;
      product.imagesURL = productModel[key].imgUrl;
      product.price = productModel[key].price; 
      product.idOrder = idOrder;

      await this.save(product);
  
    }
    
  }

}