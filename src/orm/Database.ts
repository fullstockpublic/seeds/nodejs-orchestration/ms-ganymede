import { ConnectionOptions, Connection, createConnection } from "typeorm";

console.log(`TYPEORM start on Host ${process.env.TYPEORM_HOST} | Port ${process.env.TYPEORM_PORT}`);

const connectionOpts: ConnectionOptions = {
    type: 'mongodb',
    useNewUrlParser: true,
    host: process.env.TYPEORM_HOST || 'localhost',
    port: Number(process.env.TYPEORM_PORT) || 27017,
    database: process.env.TYPEORM_DATABASE || 'ganymede',
    username: process.env.TYPEORM_USERNAME || '',
    password: process.env.TYPEORM_PASSWORD || '',  
    entities: [
      `${__dirname}/entities/*{.ts,.js}`,
    ]
  };

//const connection:Promise<Connection> = createConnection(connectionOpts);

export default connectionOpts;