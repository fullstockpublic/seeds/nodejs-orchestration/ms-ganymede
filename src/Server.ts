import "reflect-metadata";
import * as dotenv from "dotenv";
import { Container } from "typedi";
import { createKoaServer, useContainer as routingUseContainer } from "routing-controllers";
import { useContainer as typeormUseContainer, createConnection, ConnectionOptions } from "typeorm";
import Queue = require("bull");
import Arena = require("bull-arena");

if (process.env.NODE_ENV !== "production") {
    // Used only in development to load environment variables from local file.
    dotenv.config();
}

console.log(`Server start on ${process.env.NODE_ENV}`);

typeormUseContainer(Container);
routingUseContainer(Container);

//import connection from "./orm/Database";
import connectionOpts from "./orm/Database";

// creates Koa app
const app = createKoaServer({
    routePrefix: "/api/product",
    cors: true,
    controllers: [`${__dirname}/controllers/*{.ts,.js}`],
    middlewares: [`${__dirname}/middlewares/*{.ts,.js}`]
});

createConnection(connectionOpts)
  .then(async connection => {
    app.listen(process.env.PORT || 1300);
    console.log(`Server Running on ${process.env.PORT}`);
  })
  .catch(
    error => {
      console.log("Error: ", error);
    }      
  );

