import Queue = require("bull");
import * as Redis from "ioredis";

export const Themisto = 'Themisto';
export const Ganymede = 'Ganymede';

const redis = {
  port: process.env.REDIS_PORT || 6379, 
  host: process.env.REDIS_HOST || '127.0.0.1', 
  password: process.env.REDIS_PASS || 'localPass'
} as Redis.RedisOptions

export const queues = {
  [Themisto]: new Queue(
    Themisto,
    {
      redis: redis
    }    
  ),
  [Ganymede]: new Queue(
    Ganymede,
    {
      redis: redis
    }    
  )
};